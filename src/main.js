// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import translator from 'vue-translator-component'
import ptbr from './translate/pt-br'
import en from './translate/en'

Vue.config.productionTip = false

Vue.prototype.equals = function (b) {
  return this._uid === b._uid
}

Vue.use(translator)
translator.use('English', en)
translator.use('Português', ptbr)

require('./assets/css/EB_Gramond.css')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
