export default {
  'about-txt': 'Um Framework de Transformações de Modelos UML Para o Desenvolvimento de Aplicativos Para Dispositivos Moveis e Websites',
  'client': 'Cliente',
  'home': 'Inicio',
  'importCSV': 'Importar CSV',
  'insert': 'Cadastrar Novo',
  'ISSN': 'CPF',
  'list': 'Buscar & Listar',
  'name': 'Nome',
  'PID': 'RG',
  'welcome': 'Bem-vindo',
  'welcome-msg': 'Seja bem-vindo ao meusite.com'
}
