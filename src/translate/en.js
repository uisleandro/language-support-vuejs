export default {
  'about-txt2': 'Mobile and Web Model and Transformation Framework',
  'about-txt': 'A Framework of UML Model Transformation for the Develpment of Apps for Mobile Devices and Web Sites',
  'client': 'Client',
  'home': 'Home',
  'importCSV': 'Import CSV',
  'insert': 'Insert New',
  'ISSN': 'ISSN',
  'list': 'Search & List',
  'name': 'Name',
  'PID': 'ID',
  'welcome': 'Welcome',
  'welcome-msg': 'Welcome to mywebsite.com'
}
