var translator = (function () {

  var translator = function () {
    this.vues = [];
    this.dictionary = [];
    this.cultures = [];
  };

  translator.prototype.tell = function () {
    var i = this.vues.length - 1;
    while (i >= 0) {
      this.vues[i].$forceUpdate();
      i--;
    }
  };

  translator.prototype.addObserver = function (vue) {
    this.vues[this.vues.length] = vue;
  };

  translator.prototype.setCulture = function (culture) {
    if (culture !== this.culture) {
      this.culture = culture;
      this.tell();
    }
  };

	translator.prototype.getCulture = function () {
    return this.culture;
  };

  translator.prototype.getCultureArray = function () {
    return this.cultures;
  };

  translator.prototype.setLanguage = function (k, v) {
    if (typeof (this.dictionary) === 'undefined') {
      this.dictionary = [];
    }
    if (typeof (this.culture) === 'undefined') {
      this.culture = k;
    }
    this.dictionary[k] = v;
    this.cultures[this.cultures.length] = k;
  };

  translator.prototype.get = function (k) {
    if ((typeof (this.dictionary) === 'undefined') ||
        (typeof (this.culture) === 'undefined') ||
        (typeof (this.dictionary[this.culture][k]) === 'undefined')) {
      return null;
    }
    return this.dictionary[this.culture][k];
  };

  return translator;

})();

var that = this;

var vuetranslator = {
  getInstance: function (context) {
    if(typeof(context) === 'undefined'){
        context = that;
    }
    if (typeof (context.$vuetranslator_translator) === 'undefined') {
      context.$vuetranslator_translator = new translator();
    }
    return context.$vuetranslator_translator;
  },
  use: function(k, v){
    vuetranslator.getInstance().setLanguage(k, v);
  },
  install: function (Vue, options) {
    // this component will show the text
    Vue.component('v-say', {
      props: ['text'],
      template: '<span>{{ strValue }}</span>',
      data: function () {
        return {
          strValue: 'Empty Text'
        };
      },
      methods: {
        changeMyStrValue: function () {
          if (this.text) {
            var strValue = vuetranslator.getInstance().get(this.text);
            if (strValue) {
              this.strValue = strValue;
            } else {
              this.strValue = this.text;
            }
          }
        }
      },
      created: function () {
        // console.log('VML');
        vuetranslator.getInstance().addObserver(this);
      },
      mounted: function () {
        this.changeMyStrValue();
      },
      updated: function () {
        this.changeMyStrValue();
      }
    });

    // this component will change the langugage
    Vue.component('vl-select', {
      template: `<select id="s_language" v-on:change="setCulture">
  <option v-for="v in lang">{{v}}</option>
</select>`,
      data () {
        return {
          'lang': []
        }
      },
      methods: {
        setCulture: function (e) {
          // translator.getInstance().getCulture()
          let culture = document.getElementById('s_language').value
          vuetranslator.getInstance().setCulture(culture)
        }
      },
      created: function () {
        // console.log('VML2');
        // vuetranslator.getInstance().addObserver(this);
        this.lang = vuetranslator.getInstance().getCultureArray();
      },

    });

  }
};

export default vuetranslator;

