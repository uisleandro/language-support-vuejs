# language-support-vuejs

> A simple vue app with language support

* Based on Vue's webpack template
* The sidebar menu is based on the following video tutorial: https://youtu.be/uWUNZ4u1VLA
* [Simple Demonstration Page](https://uisleandro.github.io/language-support-vuejs/#/)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# The same configuration as before plus adding
# some code to run at the local network
# you must set your own ip address and hostname
# inside of package.json to make it work
npm run dev-remote

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## The vue-translator-component

Now Its possible to install the translator component without using this
little project, by typing:

```
npm install --save vue-translator-component@1.0.2
```

## Implementation Details

The translation files are located in the folder *src/translate*. 

Follows an example of the translation file content:

```
export default {
  'client': 'Client',
  'home': 'Home',
  'importCSV': 'Import CSV',
  'insert': 'Insert New',
  'ISSN': 'ISSN',
  'list': 'Search & List',
  'name': 'Name',
  'PID': 'ID',
  'welcome': 'Welcome',
  'welcome-msg': 'Welcome to mywebsite.com'
}
```

In the file *'src/main.js'* we have the following imports:

```
import translator from 'translator'
import ptbr from './translate/pt-br'
import en from './translate/en'
```

As well, In the same file, the plugin is being installed to vue:
> In the current release the 'key' must be the language name written in the language itself

```
Vue.use(translator)
translator.use('Português', ptbr)
translator.use('English', en)
```


To check the current language being used you may use:
```
translator.getInstance().getCulture()
```

To set the current language you may use:

```
translator.getInstance().setCulture('English')
```

The method **setCulture()** will update all the instances of
the 'v-say' component


## Now it's a component!!!

It may appear like this on all your components

```
<li><a href="#/"><v-say text="welcome" /></a></li>
```

If you dont set any value for text you'll get the result:

```
<li><a href="#/"><span>Empty Text</span></a></li>
```

If the word "welcome" is not set in your dictionary, the following result is expected:

```
<li><a href="#/"><span>welcome</span></a></li>
```

In another cause you may get the translated word, depending on the current language:

```
<li><a href="#/"><span>Bem-vindo</span></a></li>
```

## New Release v1.0.2

In this relase a new component has been added:

```
<vl-select />
```

It will generate a select with all the languages added before:

```
<select id="s_language"><option>Português</option><option>English</option></select>
```

## License

[MIT](https://opensource.org/licenses/MIT)
